KiCAD to Charmhigh DPV Converter
================================

### Installation

With yarn:
```bash
$ yarn global add https://dinkdonk@bitbucket.org/elektrofon/kicad-to-dpv.git
```

Or with npm:
```bash
$ npm install -g https://dinkdonk@bitbucket.org/elektrofon/kicad-to-dpv.git
```

### Usage

1. Write your feeders file. (See `examples/feeders-example.csv`)
2. Run the converter:  
```$ kicad-to-dpv convert -p path/to/KiCAD/project -f path/to/feeders.csv -o output-file-name```  
3. Load the resulting .dpv file in your pick and place machine

An optional calibration points file can also be loaded:

```bash
$ kicad-to-dpv convert -p path/to/KiCAD/project -f path/to/feeders.csv -c path/to/calibration.csv -o output-file-name
```

Example calibration file can be found at `examples/calibration-points-example.csv`.

### Help

```bash
$ kicad-to-dpv --help
```

Help for specific command
```bash
$ kicad-to-dpv <command> --help
```
