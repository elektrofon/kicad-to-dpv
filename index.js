#!/usr/bin/env node

const fs = require('fs');
const path = require('path');
const readline = require('readline');
const csv = require('fast-csv');
const yargs = require('yargs');

function statusToNumber(status) {
	// Status is a 3 bit binary where the first bit sets "skip", the second bit sets "check vacuum" and the third bit sets "use vision"
	switch (status) {
		case 0b000:
			return 0;
		case 0b100:
			return 1;
		case 0b010:
			return 2;
		case 0b110:
			return 3;
		case 0b001:
			return 4;
		case 0b101:
			return 5;
		case 0b011:
			return 6;
		case 0b111:
			return 7;
		default:
			return NaN;
	}
}

function statusToBits(status) {
	switch (status) {
		case 0:
			return 0b000;
		case 1:
			return 0b100;
		case 2:
			return 0b010;
		case 3:
			return 0b110;
		case 4:
			return 0b001;
		case 5:
			return 0b101;
		case 6:
			return 0b011;
		case 7:
			return 0b111;
		default:
			return null;
	}
}

const setSkip = async function setSkip(dpvFilePath, skip) {
	if (!fs.existsSync(dpvFilePath)) {
		console.error(`${dpvFilePath} file not found`);

		process.exit(1);
	}

	const sourceStream = fs.createReadStream(dpvFilePath);
	let targetStream;

	if (skip) {
		targetStream = fs.createWriteStream(dpvFilePath.replace('.dpv', '-skipped.dpv'));
	} else {
		targetStream = fs.createWriteStream(dpvFilePath.replace('.dpv', '-unskipped.dpv'));
	}

	const rl = readline.createInterface({
		input: sourceStream,
		crlfDelay: Infinity
	});
	// Note: we use the crlfDelay option to recognize all instances of CR LF
	// ('\r\n') in input.txt as a single line break.

	for await (const line of rl) {
		if (line.substr(0, 10) === 'EComponent') {
			// We have a component line, let's find the `status` parameter

			const lineArray = line.split(',');
			const status = parseInt(lineArray[9], 10);
			const partNumber = lineArray[12];

			if (partNumber !== 'FIDUCIAL') {
				if (skip) {
					lineArray[9] = statusToNumber(statusToBits(status) | 0b100);
				} else {
					lineArray[9] = statusToNumber(statusToBits(status) & ~0b100);
				}

				targetStream.write(lineArray.join(',') + '\n', 'utf8');
			} else {
				targetStream.write(line + '\n', 'utf8');
			}
		} else {
			targetStream.write(line + '\n', 'utf8');
		}
	}

	targetStream.end();
};

const processFeedersFile = async function processFeedersFile(path) {
	const feeders = [];

	return new Promise((resolve, reject) => {
		csv
		.fromPath(path, {
			headers: true,
			ignoreEmpty: true,
			discardUnmappedColumns: true,
			delimiter: ';'
		})
		.on('data', (data) => {
			feeders.push({
				feederId: parseInt(data['feederId'], 10),
				feedRate: parseInt(data['feedRate'], 10),
				partNumber: data['partNumber'],
				speed: 0,
				width: Number(data['width']),
				length: Number(data['length']),
				height: Number(data['height']),
				rotation: parseInt(data['rotation'], 10),
				head: parseInt(data['head'], 10),
				originalLine: Object.values(data)
			});
		})
		.on('end', () => {
			resolve(feeders);
		});
	});
};

const processCalibrationPointsFile = async function processCalibrationPointsFile(path, layoutOrigin) {
	const points = [];

	return new Promise((resolve, reject) => {
		csv
		.fromPath(path, {
			headers: true,
			ignoreEmpty: true,
			discardUnmappedColumns: true,
			delimiter: ';'
		})
		.on('data', (data) => {
			points.push({
				reference: data.reference,
				x: (Number(data.x) - layoutOrigin.x).toFixed(2),
				y: -(Number(data.y) - layoutOrigin.y).toFixed(2)
			});
		})
		.on('end', () => {
			resolve(points);
		});
	});
};

const processSchematicFile = async function processSchematicFile(path) {
	const components = [];
	let activeComponentIndex = 0;
	let readingComponent = false;
	const fileStream = fs.createReadStream(path);
	let skipComponent = false;

	const rl = readline.createInterface({
		input: fileStream,
		crlfDelay: Infinity
	});
	// Note: we use the crlfDelay option to recognize all instances of CR LF
	// ('\r\n') in input.txt as a single line break.

	for await (const line of rl) {
		if (line === '$Comp') {
			// We have a new component

			readingComponent = true;

			components.push({});
		}

		if (readingComponent) {
			const splitComponentLine = line.replace(/ +(?= )/g,'').match(/(?:[^\s"]+|"[^"]*")+/g);
			const lineType = splitComponentLine[0];
			const lineIndex = parseInt(splitComponentLine[1], 10);

			if (lineType === 'F') {
				// We have a `Field` line

				if (lineIndex === 0) {
					const reference = splitComponentLine[2].replace(/"/g, '');

					if (components.findIndex((item) => item.reference == reference) > -1) {
						process.emitWarning(`Duplicate reference ${reference} encountered. Skipping`);

						skipComponent = true;
					} else {
						skipComponent = false;
					}

					components[activeComponentIndex].reference = reference;
				}

				if (lineIndex === 1) {
					components[activeComponentIndex].value = splitComponentLine[2].replace(/"/g, '');
				}

				if (lineIndex === 2) {
					components[activeComponentIndex].footprint = splitComponentLine[2].replace(/"/g, '');
				}

				if (lineIndex === 3) {
					components[activeComponentIndex].datasheet = splitComponentLine[2].replace(/"/g, '');
				}

				if (lineIndex > 3) {
					// We have a custom field
					components[activeComponentIndex][splitComponentLine[splitComponentLine.length - 1].replace(/"/g, '')] = splitComponentLine[2].replace(/"/g, '');
				}
			}
		}

		if (readingComponent && line === '$EndComp') {
			// Component end
			readingComponent = false;

			if (!skipComponent) {
				activeComponentIndex++;
			} else {
				components.pop();
			}
		}
	}

	return components;
};

const processLayoutFile = async function processLayoutFile(path) {
	const components = [];
	let activeComponentIndex = 0;
	let readingComponent = false;
	const fileStream = fs.createReadStream(path);

	let origin = {
		x: 0,
		y: 0
	};

	const rl = readline.createInterface({
		input: fileStream,
		crlfDelay: Infinity
	});
	// Note: we use the crlfDelay option to recognize all instances of CR LF
	// ('\r\n') in input.txt as a single line break.

	for await (const line of rl) {
		if (line.substr(0, 21) === '    (aux_axis_origin ') {
			const auxOrigin = line.match(/\(aux_axis_origin (\d*) (\d*)\)/);

			if (auxOrigin) {
				origin.x = Number(auxOrigin[1]);
				origin.y = Number(auxOrigin[2]);
			}
		}

		if (line.substr(0, 9) === '  (module') {
			// We have a new component
			readingComponent = true;
			components.push({
				x: 0,
				y: 0,
				rotation: 0
			});
		}

		if (readingComponent) {
			if (line.indexOf('    (at ') === 0) {
				const splitLine = line.match(/\s*?\(at\s(\d+\.?\d*?) (\d+\.?\d*?)(?: (\d+\.?\d*?)\)|\))/);

				if (splitLine && splitLine.length >= 3) {
					components[activeComponentIndex].x = (Number(splitLine[1]) - origin.x).toFixed(2);
					components[activeComponentIndex].y = -(Number(splitLine[2]) - origin.y).toFixed(2);
					components[activeComponentIndex].rotation = Number(splitLine[3]) || 0;
				}
			}

			if (line.indexOf('    (fp_text reference ') === 0) {
				const splitLine = line.replace(/ +(?= )/g,'').split(' ');

				components[activeComponentIndex].reference = splitLine[3];
			}
		}

		if (readingComponent && line.substr(0, 3) === '  )') {
			// Component end
			readingComponent = false;
			activeComponentIndex++;
		}
	}

	return {layoutComponents: components, origin};
};

function generateDpvFeeders(feeders) {
	let output = 'Table,No.,ID,DeltX,DeltY,FeedRates,Note,Height,Speed,Status,SizeX,SizeY,HeightTake,DelayTake,nPullStripSpeed\n\n';

	for (let i = 0, l = feeders.length; i < l; i++) {
		const f = feeders[i];
		const speed = f.width * f.length >= 100 ? 50 : 0;

		output += `Station,${i},${f.feederId},0,0,${f.feedRate},${f.partNumber},${f.height},${speed},6,${(f.width * 100).toFixed(0)},${(f.length * 100).toFixed(0)},0,0,0\n`;
	}

	return output;
}

function generateDpvComponents(feeders, schematicComponents, layoutComponents, calibrationPoints, skipComponents) {
	const components = [];

	for (const component of schematicComponents) {
		const layoutComponentMatch = layoutComponents.find((item) => item.reference === component.reference);

		if (layoutComponentMatch) {
			component.position = {
				x: layoutComponentMatch.x,
				y: layoutComponentMatch.y,
				rotation: layoutComponentMatch.rotation
			};

			const feederComponentMatch = feeders.find((item) => item.partNumber === component['MFG part number']);

			if (feederComponentMatch) {
				component.position.rotation = component.position.rotation - feederComponentMatch.rotation;

				if (component.position.rotation > 180) {
					component.position.rotation = component.position.rotation - 360;
				} else if (component.position.rotation < -180) {
					component.position.rotation = component.position.rotation + 360;
				}

						if (component.reference == 'Y1') {
							console.log('Y1');
						}

				components.push({
					reference: component.reference,
					partNumber: feederComponentMatch.partNumber,
					width: feederComponentMatch.width,
					length: feederComponentMatch.length,
					height: feederComponentMatch.height,
					position: component.position,
					feederId: feederComponentMatch.feederId,
					speed: feederComponentMatch.speed,
					head: feederComponentMatch.head
				});
			} else {
				process.emitWarning(`No feeder found for ${component['MFG part number']}`);
			}
		} else {
			process.emitWarning(`Could not find ${component.reference} in layout`);
		}
	}

	let output = 'Table,No.,ID,PHead,STNo.,DeltX,DeltY,Angle,Height,Skip,Speed,Explain,Note,Delay\n\n';
	let status = statusToNumber(0b011); // Skip 0, check vacuum 1, use vision 1

	if (skipComponents) {
		status = statusToNumber(0b111); // Skip 1, check vacuum 1, use vision 1
	}

	let i = 0;

	if (calibrationPoints) {
		output += `EComponent,0,1,1,1,${calibrationPoints[0].x},${calibrationPoints[0].y},0,0,${statusToNumber(0b100)},0,${calibrationPoints[0].reference},FIDUCIAL,0\n`;

		i++;
	}

	for (i = calibrationPoints ? 1 : 0, l = components.length; i < l; i++) {
		output += `EComponent,${i},${i + 1},${components[i].head},${components[i].feederId},${components[i].position.x},${components[i].position.y},${components[i].position.rotation},${components[i].height},${status},${components[i].width * components[i].length < 100 ? 0 : 50},${components[i].reference},${components[i].partNumber},0\n`;
	}

	if (calibrationPoints) {
		i = components.length;

		output += `EComponent,${i},${i + 1},1,1,${calibrationPoints[1].x},${calibrationPoints[1].y},0,0,${statusToNumber(0b100)},0,${calibrationPoints[1].reference},FIDUCIAL,0\n`;
	}

	return {dpvComponents: output, dpvComponentsCount: components.length};
}

const convert = async function convert(projectPath, feedersFile, outputFileName, calibrationPointsFile, skipComponents) {
	const projectFiles = fs.readdirSync(projectPath);
	let schematicFile;
	let layoutFile;

	for (const file of projectFiles) {
		if (path.extname(file) === '.sch') {
			schematicFile = path.join(projectPath, file);
		}

		if (path.extname(file) === '.kicad_pcb') {
			layoutFile = path.join(projectPath, file);
		}
	}

	if (!schematicFile) {
		console.error('`*.sch` file not found in project directory!');

		process.exit(1);
	}

	if (!layoutFile) {
		console.error('`*.kicad_pcb` file not found in project directory!');

		process.exit(1);
	}

	if (!fs.existsSync(feedersFile)) {
		console.error(`${feedersFile} file not found!`);

		process.exit(1);
	}

	const feeders = await processFeedersFile(feedersFile);
	const schematicComponents = await processSchematicFile(schematicFile);
	const {layoutComponents, origin} = await processLayoutFile(layoutFile);
	let calibrationPoints;
	let calibrationPointsOutput = '';

	if (calibrationPointsFile) {
		calibrationPoints = await processCalibrationPointsFile(calibrationPointsFile, origin);
	}

	const dpvFeeders = generateDpvFeeders(feeders);
	const {dpvComponents, dpvComponentsCount} = generateDpvComponents(feeders, schematicComponents, layoutComponents, calibrationPoints, skipComponents);

	if (calibrationPointsFile) {
		calibrationPointsOutput = `CalibPoint,0,1,${calibrationPoints[0].x},${calibrationPoints[0].y},Mark1
CalibPoint,1,2,${calibrationPoints[1].x},${calibrationPoints[1].y},Mark2\n\n`;
	}

	const now = new Date().toISOString().replace(/-/g, '/').replace('T', ' ').split(' ');

	let output = `separated
FILE,${outputFileName}.dpv
PCBFILE,${path.basename(layoutFile)}
DATE,${now[0]}
TIME,${now[1]}
PANELTYPE,0

${dpvFeeders}


Table,No.,ID,DeltX,DeltY

Panel_Coord,0,1,0,0


${dpvComponents}


Table,No.,nType,nAlg,nFinished

PcbCalib,0,1,0,1

Table,No.,ID,offsetX,offsetY,Note

${calibrationPointsOutput}
Table,No.,DeltX,DeltY,AlphaX,AlphaY,BetaX,BetaY,DeltaAngle

CalibFator,0,0,0,0,0,0,1,0
	`;

	fs.writeFileSync(`${path.join(process.cwd(), outputFileName + '.dpv')}`, output);
};

yargs
.usage('$0 <command> [options]')
.command('convert', 'Converts KiCAD project to .dpv for pick and place machine', async (yargs) => {
	const argv = yargs
	.option('p', {
		description: 'The path to where your .sch and .kicad_pcb files are located',
		default: process.cwd(),
		alias: 'kicadProjectPath'
	})
	.option('f', {
		description: 'The path to where your feeders.csv is located',
		default: 'examples/feeders-example.csv',
		alias: 'feedersPath'
	})
	.option('c', {
		description: 'The calibration points (optional)',
		alias: 'calibrationPointsPath'
	})
	.option('o', {
		description: 'The name you want to give the output file',
		default: 'output',
		alias: 'outputFileName'
	})
	.option('s', {
		description: 'Set skip on all components (great for testing placement of components one by one)',
		default: false,
		alias: 'skipComponents'
	})
	.help()
	.argv;

	try {
		await convert(argv.kicadProjectPath, argv.feedersPath, argv.outputFileName, argv.calibrationPointsPath, argv.skipComponents);
	} catch (error) {
		console.error(error);
	}
})
.command('unskip [file]', 'Sets skip to false for all components', async (yargs) => {
	const argv = yargs
	.positional('f', {
		description: 'The path to a .dpv file',
		alias: 'file'
	})
	.demandOption(['file'])
	.help()
	.argv;

	try {
		await setSkip(argv.file, false);
	} catch (error) {
		console.error(error);
	}
})
.command('skip [file]', 'Sets skip to true for all components', async (yargs) => {
	const argv = yargs
	.positional('f', {
		description: 'The path to a .dpv file',
		alias: 'file'
	})
	.demandOption(['file'])
	.help()
	.argv;

	try {
		await setSkip(argv.file, true);
	} catch (error) {
		console.error(error);
	}
})
.help()
.argv;